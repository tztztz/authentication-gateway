defmodule Authentication_gateway.Server do
  use Plug.Builder
  require Logger

  plug(:authenticate)
  plug(Authentication_gateway.Router)

  def authenticate(%Plug.Conn{request_path: request_path} = conn, _opts) do
    token = get_token(request_path)

    if valid_token(token) do
      Logger.info("valid token!")

      conn
    else
      Logger.info("invalid token!")

      conn
      # |> put_resp_header(
      #   "Location",
      #   "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
      # )
      |> send_resp(401, "")
      |> halt()
    end
  end

  defp get_token(request_path) do
    [token | _path] =
      request_path
      |> String.slice(1..-1)
      |> String.split("/")

    token
  end

  defp authorized_tokens() do
    System.get_env("GATEWAY_AUTHORIZED_TOKENS")
    |> String.split(",")
    |> authorized_tokens()
  end

  defp authorized_tokens([]) do
    Logger.error("Please set GATEWAY_AUTHORIZED_TOKENS environment variable")

    []
  end

  defp authorized_tokens(tokens) do
    tokens
  end

  defp valid_token(given_token) do
    case authorized_tokens()
         |> Enum.find(&String.equivalent?(&1, given_token)) do
      nil -> false
      _ -> true
    end
  end
end
