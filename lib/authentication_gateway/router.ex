defmodule Authentication_gateway.Router do
  use Plug.Router
  require Logger

  plug(:match)
  plug(:dispatch)

  def endpoint(path) do
    gateway_endpoint = System.get_env("GATEWAY_ENDPOINT")

    URI.merge(gateway_endpoint, Enum.join(path, "/"))
    |> to_string()
  end

  match "/:token/*path" do
    opts = PlugProxy.init(url: endpoint(path))

    PlugProxy.call(conn, opts)
  end
end
