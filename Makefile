#@name Authentication_gateway
#@description A gateway to access a tezos node with a magic auth link

# Here we ensure that every command this Makefile run will run in a bash shell
# instead of the default sh
# See https://tech.davis-hansson.com/p/make
SHELL := /usr/bin/env bash

source = source env.sh 2>&1 > /dev/null || true

APP_NAME ?= $(shell grep 'app:' mix.exs | sed -e 's/\[//g' -e 's/ //g' -e 's/app://' -e 's/[:,]//g')
APP_VSN ?= $(shell grep 'version:' mix.exs | cut -d '"' -f2 )
BUILD ?= $(shell git rev-parse --short HEAD )


deps: mix.exs mix.lock
	@mix deps.get
	@touch $@

.DEFAULT_GOAL := interactive
## Starts Authentication_gateway on http://localhost:3000
interactive: deps
	${source} && iex -S mix

.PHONY: build
build:
	mix compile

RELEASE_PATH = "_build/$(MIX_ENV)/rel/$(APP_NAME)/releases/$(APP_VSN)/"

.PHONY: release
release:
	MIX_ENV=prod mix release --path $(RELEASE_PATH)

.PHONY: test
test:
	MIX_ENV=test mix test

.PHONY: clean
clean:
	rm -rf deps
	rm -rf _build

# Docker Section
# --------------
#
# This is dedicated to target that are docker related

## Build the Docker image
TAG = $(APP_VSN)-$(BUILD)
BUILDER_IMG = $(APP_NAME)-builder
TESTER_IMG = $(APP_NAME)-tester

.PHONY: docker-build
docker-build:
	@echo "🐳 Build the docker image"
	${source}
	docker build \
			--rm=false \
			--build-arg APP_NAME=$(APP_NAME) --build-arg APP_VSN=$(APP_VSN) \
			-t $(BUILDER_IMG):$(TAG)                      \
			-t $(BUILDER_IMG):latest                                   \
			--target builder .

.PHONY: docker-test
docker-test: #docker-build
	@echo "🐳 Test the docker image"
	${source}
	#docker build \
	#    	--build-arg APP_NAME=$(APP_NAME) --build-arg APP_VSN=$(APP_VSN) \
	#		-t $(TESTER_IMG):$(TAG)                       \
	#		-t $(TESTER_IMG):latest                                    \
	#		--target tester .
	docker run -e MIX_ENV=test  $(BUILDER_IMG) make test

.PHONY: docker-release
docker-release:
	@echo "🐳 Create a production docker image"
	${source} && docker build \
	    	--build-arg APP_NAME=$(APP_NAME) --build-arg APP_VSN=$(APP_VSN) \
			-t $(APP_NAME):$(APP_VSN)-$(BUILD)                              \
			-t $(APP_NAME):latest                                           \
			--target production .

.PHONY: docker-serve
## Run the app in Docker
docker-serve: # docker-release
	@echo "🐳 Run Project Saturn in docker"
	${source} && docker run \
		-e GATEWAY_AUTHORIZED_TOKENS="$${GATEWAY_AUTHORIZED_TOKENS}"   \
	  -e GATEWAY_ENDPOINT="$${GATEWAY_ENDPOINT}"   \
    -p 3000:3000        \
    --rm -it $(APP_NAME):latest

bin/pretty-make:
	@curl -Ls https://raw.githubusercontent.com/awea/pretty-make/master/scripts/install.sh | bash -s

.PHONY: help
## List available commands
help: bin/pretty-make
	@bin/pretty-make pretty-help Makefile
