defmodule Authentication_gateway.MixProject do
  use Mix.Project

  def project do
    [
      app: :authentication_gateway,
      version: "0.1.0",
      elixir: "~> 1.12",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Authentication_gateway, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:plug, "~> 1.0"},
      {:plug_cowboy, "~> 2.0"},
      {:plug_proxy, git: "https://github.com/Awea/plug-proxy", branch: "latest-deps"}
    ]
  end
end
